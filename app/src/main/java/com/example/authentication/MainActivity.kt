package com.example.authentication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import java.util.regex.Pattern

class MainActivity : AppCompatActivity() {

    private lateinit var emailText: EditText
    private lateinit var passwordText: EditText
    private lateinit var confirmPasswordText: EditText
    private lateinit var submitButton: Button

    private val emailAddressPattern: Pattern = Pattern.compile(
        "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")+"
    )
    private val passwordPattern: Pattern = Pattern.compile(
        "^(?=.*[0-9])(?=.*[a-z]).{9,}"
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        emailText = findViewById(R.id.editTextEmail)
        passwordText = findViewById(R.id.editTextPassword)
        confirmPasswordText = findViewById(R.id.editTextConfirmPassword)
        submitButton = findViewById(R.id.buttonSubmit)

        submitButton.setOnClickListener {
            val email = emailText.text.toString().trim()
            val password = passwordText.text.toString().trim()
            val confirmPassword = confirmPasswordText.text.toString().trim()



            if (email.isEmpty() && password.isEmpty() && confirmPassword.isEmpty()) {
                emailText.error = "გრაფა ცარიელია"
                passwordText.error = "გრაფა ცარიელია"
                confirmPasswordText.error = "გრაფა ცარიელია"
                return@setOnClickListener
            }
            if (email.isEmpty()) {
                emailText.error = "გრაფა ცარიელია"
                return@setOnClickListener
            }
            if (!(emailAddressPattern.matcher(email).matches())) {
                emailText.error = "მეილი არასწორადაა შეყვანილი"
                return@setOnClickListener
            }
            if (password.isEmpty()) {
                passwordText.error = "გრაფა ცარიელია"
                return@setOnClickListener
            }
            if (password.length <= 9) {
                passwordText.error = "პაროლი უნდა შედგებოდეს მინიმუმ 9 სიმბოლოსაგან"
                return@setOnClickListener
            }
            //მეორე გზა
//            if ((!password.contains("0") && !password.contains("1") && !password.contains("2") &&
//                        !password.contains("3") && !password.contains("4") && !password.contains("5") &&
//                        !password.contains("6") && !password.contains("7") && !password.contains("8") &&
//                        !password.contains("9"))
//            ) {
//                passwordText.error = "პაროლი უნდა შეიცავდეს მინიმუმ ერთ ციფრს"
//                return@setOnClickListener
//            }
            if (!(passwordPattern.matcher(password).matches())) {
                passwordText.error = "პაროლი უნდა შეიცავდეს მინიმუმ ერთ ციფრსა და ალფაბეტს"
                return@setOnClickListener
            }
            if (confirmPassword.isEmpty()) {
                confirmPasswordText.error = "გრაფა ცარიელია"
                return@setOnClickListener

            }

            if (confirmPassword != password) {
                confirmPasswordText.error = "პაროლი არ ემთხვევა"
                return@setOnClickListener
            } else {
                FirebaseAuth.getInstance()
                    .createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener { task ->
                        if (task.isSuccessful) {
                            Toast.makeText(
                                this,
                                "სისტემაში წარმატებით შეხვედით",
                                Toast.LENGTH_SHORT
                            ).show()
                        } else {
                            Toast.makeText(this, "Failed", Toast.LENGTH_SHORT).show()
                        }

                    }

            }
        }

    }
}

